<?php
include("Config.php");
require_once("DBConnection.php");
class Estabelecimentos {
	
	public $database;

	public function __construct() {
		$dbconfig = new Config();
		$this->database = new DBConnection($dbconfig->getConfig());
	}

	public function getDetalhes($token) {

		$sqlSelect = "SELECT * FROM estabelecimentos WHERE token = '$token'";   
		$rows = $this->database->getQuery($sqlSelect); 
		$i = 0;

		foreach($rows as $row){
			$arrayEstabelecimentos[$i] = array('token' => $row['token'],
											'cnpj' => $row['cnpj'],
											'data_hora' => $row['data_hora'],
											'setor' => $row['setor'],
											'logo' => $row['logo'],
											'horario_abertura' => $row['horario_abertura'],
											'horario_fechamento' => $row['horario_fechamento'],
											'nome_fantasia' => $row['nome_fantasia'],
											'latitude' => $row['latitude'],
											'longitude' => $row['longitude'],
											'is_open' => $this->isEstabelecimentoOpen($row['horario_abertura'], $row['horario_fechamento']),
											'distance' => $this->distance(floatval($row['latitude']),floatval($row['longitude']),  $currentLatitude, $currentLongitude),
											'razao_social' => $row['razao_social'],
											'ramo' => $row['ramo'],
											'fone_comercial' => $row['telefone_comercial'],
											'fone_whatsapp' => $row['telefone_whatsapp'],
											'fone_alternativo' => $row['telefone_alternativo'],
											'nota_empresa' => $row['nota'],
											'passa_cartao' => $row['passa_cartao'],
											'descricao' => $row['descricao'],
											'faz_entrega' => $row['entrega']
											);
			$i++;

		}

		echo json_encode ($arrayEstabelecimentos);

	}

	private function isEstabelecimentoOpen($horarioInicial, $horarioFinal) {
		$isOpen = false;
		date_default_timezone_set('America/Sao_Paulo');
		$datetime = new DateTime();
		$currentTime = $datetime->format('H:i:s');
		
		if ((strtotime($horarioInicial) <= strtotime($currentTime)) && (strtotime($horarioFinal) >= strtotime($currentTime))){
			$isOpen = true;
		}else{
			$isOpen = false;
		}

		return $isOpen;
	}

	function distance($lat1, $lon1, $lat2, $lon2) {

	  	$theta = $lon1 - $lon2;
    	$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
                              $dist = acos($dist);
                              $dist = rad2deg($dist);
                              $miles= $dist * 60 * 1.1515;
                              $unit = 'K';
                              $km   = $miles*1.609344;
                              return number_format($km,1);
	}


	public function getListagem($currentLatitude, $currentLongitude) {

		$sqlSelect = 'SELECT * FROM estabelecimentos';   
		$rows = $this->database->getQuery($sqlSelect); 
		$i = 0;

		foreach($rows as $row){
			$arrayEstabelecimentos[$i] = array('token' => $row['token'],
											'logo' => $row['logo'],
											'nome_fantasia' => $row['nome_fantasia'],
											'is_open' => $this->isEstabelecimentoOpen($row['horario_abertura'], $row['horario_fechamento']),
											'distance' => $this->distance(floatval($row['latitude']),floatval($row['longitude']),  $currentLatitude, $currentLongitude),
											'ramo' => $row['ramo'],
											'passa_cartao' => $row['passa_cartao'],
											'faz_entrega' => $row['entrega']
											);
			$i++;

		}

		echo json_encode ($arrayEstabelecimentos);

	}

	

//end of class
}
	
?>